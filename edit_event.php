<?php
include('edit_event_data.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Event Managment </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style>
  .required{
	  color:red;
  }
  </style>
  
</head>
<body>

<div class="container">
  <h2>Edit Event</h2>

  <form action="" method="post">
    <div class="form-group">
     <label>Event Title </label><span class="required">*</span> :
    <input type="text" name="title" id="title" value="<?php echo $data['title']?>" >
    </div>
	
    <div class="form-group">
     <label>Event Start Date </label><span class="required">*</span> :
     <input type="date" name="start_date" id="start_date" value="<?php echo $data['start_date']?>" >
    </div>
	
	<div class="form-group">
	<label>Event End Date </label><span class="required">*</span> :
	<input type="date" name="end_date" id="end_date" value="<?php echo $data['end_date']?>" >
    </div>
	
	
	
	<div class="form-group">
		<label>Recurrence </label><span class="required">*</span> :
		<select name="recurrence_every" id="recurrence_every">
		<option value="<?php echo $data['recurrence_every']?>"><?php echo $data['recurrence_every']?></option>
		<option value="Every">Every</option>
		<option value="Every Other">Every Other</option>
		<option value="Every Third">Every Third</option>
		<option value="Every Fourth">Every Fourth</option>
		</select>

		<select name="recurrence_day" id="recurrence_day">
		<option value="<?php echo $data['recurrence_day']?>"><?php echo $data['recurrence_day']?></option>
		<option  value="Day">Day</option>
		<option value="Week">Week</option>
		<option value="Month">Month</option>
		<option value="Year">Year</option>
		</select>
    </div>
	
    <button type="submit" class="btn btn-default" name="submit" id="submit">Save</button>
	 <a href="index.php" class="btn btn-default" name="submit" id="submit">Close</a>
  </form>
</div>

</body>
</html>
