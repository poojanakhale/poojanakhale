<?php
include('add_event_data.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Event Managment </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style>
  .required{
	  color:red;
  }
  </style>
  
</head>
<body>

<div class="container">
  <h2>Add Event</h2>
  <p style="color:red;align:center;"><?php echo $msg; ?></p>
  <form action="" method="post">
    <div class="form-group">
     <label>Event Title </label><span class="required">*</span> :
    <input type="text" name="title" id="title" >
    </div>
	
    <div class="form-group">
     <label>Event Start Date </label><span class="required">*</span> :
     <input type="date" name="start_date" id="start_date" >
    </div>
	
	<div class="form-group">
	<label>Event End Date </label><span class="required">*</span> :
	<input type="date" name="end_date" id="end_date">
    </div>
	
	
	
	<div class="form-group">
		<label>Recurrence </label><span class="required">*</span> :
		<select name="recurrence_every" id="recurrence_every">
		
		<option selected="selected" value="1">Every</option>
		<option value="2">Every Other</option>
		<option value="3">Every Third</option>
		<option value="4">Every Fourth</option>
		</select>

		<select name="recurrence_day" id="recurrence_day">
		<option selected="selected" value="1">Day</option>
		<option value="7">Week</option>
		<option value="30">Month</option>
		<option value="365">Year</option>
		</select>
    </div>
	
    <button type="submit" class="btn btn-default" name="submit" id="submit">Submit</button>
	 <a href="index.php" class="btn btn-default" name="submit" id="submit">Close</a>
  </form>
</div>

</body>
</html>
