<html>
<head>
<style>

table{
	width:100%;
}
th,td{
	border:1px;
	text-align:center;
	border:1px solid black;
}
tr:nth-child(even){
	background:gray;
}
.add{
	    background: black;
    color: white;
    padding: 5px;
    float: left;
}
</style>
</head>
<body>
<h4>Even List</h4>
<a href="add_event.php" class="add">+ Add Event</a></br>
<table>
<tr>
<th>Title</th>
<th>Dates</th>
<th>Occurrence</th>
<th>Actions</th>
</tr>
<?php
include('config.php');
if(isset($_GET['delete'])){
	mysqli_query($conn,"delete from event where id =".$_GET['delete']);
}
$query= "select * from event order by id desc";
$get_data= mysqli_query($conn,$query);
// print_r($get_data);
// exit;
//$row = mysqli_fetch_array($get_data);
while($row = mysqli_fetch_assoc($get_data))
{
?>
<tr>
<td><?php  echo $row['title'];?></td>
<td><?php  echo $row['start_date'].' to '. $row['end_date'];?></td>
<td><?php  if($value1= $row['recurrence_every']==1){
	$value1="Every";
}else if($value1= $row['recurrence_every']==2){
	$value1="Every";
}else if($value1= $row['recurrence_every']==3){
	$value1="Every Other";
}else if($value1= $row['recurrence_every']==4){
	$value1="Every Third";
}

if($value11= $row['recurrence_day']==1){
	$value11="Day";
}else if($value1= $row['recurrence_day']==7){
	$value11="Week";
}else if($value1= $row['recurrence_day']==30){
	$value11="Month";
}else if($value1= $row['recurrence_day']==365){
	$value11="Year";
}


	echo $value1.'  '. $value11?></td>
<td> <a href="view.php?view=<?php echo $row['id'];?>">View</a>&nbsp;&nbsp;
<a href="edit_event.php?edit=<?php echo $row['id'];?>">Edit</a>  &nbsp;&nbsp;
<a href="index.php?delete=<?php echo $row['id'];?>">Delete</a></td>
</tr>
<?php 
}
?>
</table>
</body>
</html>